const express = require('express')
const handler = require('./handler')

const PORT = (process.env.PORT || 5000)
const app = express()

app.get('/', (req, res) => res.send(handler()))

module.exports = () => new Promise((resolve, reject) => {
  const server = app.listen(PORT, () => {
    console.log(`Up and running at :${PORT}`)
    resolve(server)
  })
})
