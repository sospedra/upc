/* global describe, expect, it */
const handler = require('../lib/handler')

describe('Unit suite', () => {
  it('should answer hello world', () => {
    expect(handler()).toBe('Hello David!')
  })
})
