/* global afterAll, describe, expect, it */
const request = require('request')

const createServer = require('../lib/server')

describe('Integration suite', () => {
  let server = null

  afterAll(() => {
    server.close()
  })

  it('should start the server', () => {
    return createServer().then((s) => {
      server = s
      expect(true)
    })
  })

  it('should return hello world at root', (done) => {
    request('http://127.0.0.1:5000', (err, response, body) => {
      expect(!err)
      expect(body).toBe('Hello David!')
      done()
    })
  })
})
